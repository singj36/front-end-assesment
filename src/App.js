import React, {
  useState,
  useEffect
} from 'react';
import logo from './logo.svg';
import ReactDOM from "react-dom"
import './App.css';

function StudentAssess() {
  const [data, setData] = useState(null);
  useEffect(() => {
    fetch(`https://www.hatchways.io/api/assessment/students`)
      .then(res => res.json())
      .then(setData)
      .catch(console.error)
  }, [])
  if (data) {
    return (

      <div>{
        data.students.map((student,i) => {
          return (<> <div key={i}>
            <img className = "pich"src={student.pic}></img><br />
            <div className = "right">
            <h1 className="nameh"><span>
            {student.firstName}  </span>
         <span>  {student.lastName} 
           </span></h1>   
           <span class="names">
           Email : {student.email}
           </span><br />
           <span class="names">
           Company : {student.company}
           </span><br />
           <span class="names">
           Skill: {student.skill}
           </span><br />
           <span class="names">
          Average : {Average(student.grades)} %
           </span><br />
           

            </div>
            </div>
            </>
            
            )
        })
      }
      </div>
      /* <
      div ><h1> {
        data.students.length
        //data.keys("students").length();
        //data.students[0].city
        
      } </h1>< /div>*/
    )
  }
  return null;
}
function Average(grades){
  var l=grades.length;
  var total =0;
  for (var i = 0; i < l; i++  ){
    total +=parseInt(grades[i])
  }
 return total/l;
 

}

function App() {
  return (
    StudentAssess()

  );
}

export default App;